use bevy::prelude::*;

pub struct LavaPlugin;

#[derive(Resource)]
pub struct LavaLevel{
	pub level: f32,
	speed: f32
}

#[derive(Component)]
struct LavaVisualizer;

impl Plugin for LavaPlugin{
	fn build(&self, app: &mut App) {
		app.insert_resource(LavaLevel{ level: -40.0, speed: 3.5 });
		app.add_systems(Startup, initialize);
		app.add_systems(Update, handle_rising_lava);
		app.add_systems(Update, visualize_lava.after(handle_rising_lava));
	}
}

fn initialize(mut commands: Commands){
	commands.spawn((
		SpriteBundle {
			transform: Transform {
				translation: Vec3::new(0.0, -50.0, -1.0),
				scale: Vec3::new(50.0, 100.0, 1.0),
				..Default::default()
			},
			sprite: Sprite {
				color: Color::ORANGE_RED,
				..Default::default()
			},
			..Default::default()
		},
		LavaVisualizer
	));
}

fn handle_rising_lava(mut lava_level: ResMut<LavaLevel>, time: Res<Time>){
	let dt = time.delta_seconds();
	lava_level.level += dt * lava_level.speed;
}

fn visualize_lava(lava: Res<LavaLevel>, mut query: Query<&mut Transform, With<LavaVisualizer>>){
	for mut transform in &mut query {
		transform.translation.y = lava.level - transform.scale.y * 0.5;
	}
}