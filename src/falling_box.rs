use bevy::prelude::*;
use rand::Rng;
use crate::{collision::{self, Collider, ColliderEvent}, physics::Velocity};

#[derive(Resource)]
struct BoxSpawnTimer{
	time_left: f32,
	min_height: f32
}

pub struct FallingBoxPlugin;

#[derive(Component)]
pub struct StoppedFalling;

impl Plugin for FallingBoxPlugin{
	fn build(&self, app: &mut App) {
		app.insert_resource(
			BoxSpawnTimer{
				time_left: 0.0,
				min_height: 0.0
			}
		);
		app.add_systems(Update, handle_stop_falling_markers);
		app.add_systems(Update, handle_box_spawning.after(handle_stop_falling_markers));
	}
}

#[derive(Component)]
struct FallingBox;

fn handle_box_spawning(
	mut commands: Commands, time: Res<Time>, mut timer: ResMut<BoxSpawnTimer>,
	cam_query: Query<(&Transform, &Camera), With<Camera2d>>, colliders: Query<&Collider, With<Velocity>>
) {

	let cam = cam_query.single().0;
	let dt = time.delta_seconds();
	timer.time_left -= dt;
	
	// if timer is finished
	if timer.time_left <= 0.0 {
		create_random_box(&mut commands, cam, &colliders);
		let mut rng = rand::thread_rng();
		timer.time_left = rng.gen_range(0.5 .. 2.5);
	}
}

fn create_random_box(
	commands: &mut Commands, cam_transform: &Transform, 
	others: &Query<&Collider, With<Velocity>>
) {
	let mut rng = rand::thread_rng();

	let min_box_size = 6.0;
	let max_box_size = 10.0;
	let unique_sizes = 4.0;
	let box_size = 
		((rng.gen_range::<f32, _>(0.0 .. unique_sizes).floor()) / (unique_sizes - 1.0)) * 
		(max_box_size - min_box_size) + min_box_size;

	let x_range = 20.0 - (max_box_size * 0.5 + 1.0);
	let unique_locations = 8.0;
	let x_pos = 
		-x_range + 
		(
			(rng.gen_range::<f32, _>(1.01 .. unique_locations + 0.99).floor() - 1.0) / 
			(unique_locations - 1.0)
		) 
		* x_range * 2.0;

	let cam_top = cam_transform.translation.y + 50.0;
	let mut y_pos = cam_top - box_size * 0.5;
	let mut y_valid = false;
	let mut box_rect = Rect::from_center_half_size(Vec2::new(x_pos, y_pos) , Vec2::splat(box_size * 0.5));

	// adjust y so that it's not overlapping any falling boxes
	while !y_valid {
		y_valid = true;
		for other_col in others {
			let col = other_col.overlaps(&box_rect);
			if let Some(col) = col {
				y_valid	= false;
				let y_adjust = col.intersect.height() + min_box_size;
				y_pos += y_adjust;
				box_rect.max.y += y_adjust;
				box_rect.min.y += y_adjust;
			}
		}
	}

	commands.spawn(
		FallingBoxBundle{
			sprite_bundle: SpriteBundle {
				transform: Transform{
					translation: Vec3::new(x_pos, y_pos, 0.0),
					scale: Vec3::new(box_size, box_size, 1.0),
					..Default::default()
				},
				sprite: Sprite{
					color: Color::WHITE,
					..Default::default()
				},
				..Default::default()
			},
			velocity: Velocity(Vec2::new(0.0, -15.0)),
			..Default::default()
		}
	);
}

fn handle_stop_falling_markers(
	mut timer: ResMut<BoxSpawnTimer>, query: Query<&Collider, Added<StoppedFalling>>
) {
	for col in &query {
		timer.min_height = timer.min_height.max(col.max.y);
	}
}

fn box_on_collide(
	commands: &mut Commands,
	_col: &collision::Collision,
	entity: &Entity
){
	commands.entity(*entity).remove::<(Velocity, ColliderEvent)>();
	commands.entity(*entity).insert(StoppedFalling);
}

#[derive(Bundle)]
struct FallingBoxBundle{
	sprite_bundle: SpriteBundle,
	velocity: Velocity,
	collider: Collider,
	collider_evt: ColliderEvent
}

impl Default for FallingBoxBundle{
	fn default() -> Self {
		FallingBoxBundle { 
			sprite_bundle: default(),
			velocity: default(),
			collider: Collider(Rect::default()),
			collider_evt: ColliderEvent { 
				on_static_collide: Some(box_on_collide), collisions: Vec::new()
			}
		}
	}
}