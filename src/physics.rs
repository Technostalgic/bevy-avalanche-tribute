use bevy::prelude::*;
pub mod gravity;
pub mod linear_damping;

pub struct PhysicsPlugin;

impl Plugin for PhysicsPlugin {
	fn build(&self, app: &mut App) {
		app.add_plugins((
			gravity::GravityPlugin{ ..Default::default() },
			linear_damping::LinearDampingPlugin
		));
		app.add_systems(Update, handle_velocity.after(gravity::handle_gravity));
	}
}

#[derive(Component, Default, Deref, DerefMut)]
pub struct Velocity(pub Vec2);

fn handle_velocity(time: Res<Time>, mut query: Query<(&mut Transform, &Velocity)>) {
	let dt = time.delta_seconds();
	for (mut transform, velocity) in &mut query {
		transform.translation.x += velocity.0.x * dt;
		transform.translation.y += velocity.0.y * dt;
	}
}