use bevy::prelude::*;
use crate::{
	physics::{ self, Velocity, gravity::Gravity, linear_damping::LinearDamping }, 
	collision::{Collider, DynamicColliderReaction, self, ColliderEvent}, lava::LavaLevel
};

pub struct PlayerPlugin;

#[derive(Resource)]
pub struct Score{
	pub height: u32,
	pub time: f32
}

#[derive(Component)]
pub struct ScoreVisualizer;

impl Plugin for PlayerPlugin {
	fn build(&self, app: &mut App){
		app.insert_resource(Score{ height: 0, time: 0.0 });
		app.add_systems(Startup, initialize);
		app.add_systems(FixedUpdate, handle_grounded_timer.after(collision::check_collisions));
		app.add_systems(FixedUpdate, handle_ground_detection.after(handle_grounded_timer));
		app.add_systems(Update, handle_controller);
		app.add_systems(Update, handle_wrapping);
		app.add_systems(Update, handle_score_visualizer.after(handle_controller));
	}
}

#[derive(Component)]
pub struct Controller;

#[derive(Component)]
pub struct GroundDetector{
	pub on_ground: bool,
	ground_reset_timer: f32
}

#[derive(Component)]
pub struct WrapAroundX{
	pub range: f32
}

fn initialize(mut commands: Commands){

	// player properties
	let player_col = Color::Rgba { red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0 };
	let player_size = 2.0;

	// create player
	commands.spawn((
		SpriteBundle{
			transform: Transform{
				translation: Vec3::new(0.0, 5.0, 1.0),
				scale: Vec3::new(player_size, player_size * 1.5, 1.0),
				..Default::default()
			},
			sprite: Sprite{
				color: player_col,
				..Default::default()
			},
			..Default::default()
		},
		Controller,
		WrapAroundX{ range: 20.0 + player_size * 0.5, },
		Velocity(Vec2::default()),
		Gravity{ gravity_scale: 10.0 },
		LinearDamping(Vec2::splat(0.5)),
		ColliderEvent{ collisions: Vec::new(), on_static_collide: None },
		Collider(Rect::default()),
		DynamicColliderReaction,
		GroundDetector{ ground_reset_timer: 0.0, on_ground: false }
	));

	commands.spawn((
		SpriteBundle {
			transform: Transform {
				translation: Vec3::new(0.0, 0.0, -0.5),
				scale: Vec3::new(50.0, 0.5, 1.0),
				..Default::default()
			},
			sprite: Sprite {
				color: Color::DARK_GREEN,
				..Default::default()
			},
			..Default::default()
		},
		ScoreVisualizer
	));
}

fn handle_controller(
	mut commands: Commands,
	time: Res<Time>, 
	fixed_time: Res<FixedTime>,
	input: Res<Input<KeyCode>>, 
	world_gravity: Res<physics::gravity::GravityAcceleration>,
	lava: Res<LavaLevel>,
	mut score: ResMut<Score>,
	mut query: Query<(
		Entity, &mut Sprite, &mut physics::Velocity, &collision::Collider,
		&collision::ColliderEvent, &physics::gravity::Gravity, 
		&mut physics::linear_damping::LinearDamping, &mut GroundDetector
		), With<Controller>>
) {
	let move_left = input.pressed(KeyCode::Left);
	let move_right = input.pressed(KeyCode::Right);
	let move_up = input.pressed(KeyCode::Up);
	let move_down = input.pressed(KeyCode::Down);
	let move_jump = input.just_pressed(KeyCode::Up);

	let max_drop_speed = -40.0;
	let max_move_speed = 30.0;
	let acceleration = 150.0;
	let jump_sustain = 0.5;
	let dt = time.delta().as_secs_f32().min(0.05);

	let mut is_alive = false;

	for (
		entity, mut sprite, mut velocity, collider, collider_event, gravity_component, 
		mut linear_damping, mut grounded
	) in &mut query {

		// check if sqaushed
		if grounded.on_ground {
			for collision in &collider_event.collisions {

				// ceiling collision
				if collision.intersect.width() > collision.intersect.height(){
					let collision_center = collision.intersect.center();
					if collision_center.y > collider.center().y {
						commands.entity(entity).remove::<(Velocity, Collider)>();
						sprite.color = Color::rgb(1.0, 0.0, 0.0);
						return;
					}
				}
			}
		}

		// check if in lava
		if collider.min.y < lava.level {
			commands.entity(entity).remove::<(Velocity, Collider)>();
			sprite.color = Color::rgb(1.0, 0.0, 0.0);
			return;
		}

		// update score
		score.height = (collider.max.y.floor() as u32).max(score.height);

		let mut movement = Vec2::ZERO;

		if move_left {
			movement.x -= 1.0;
		}
		if move_right {
			movement.x += 1.0;
		}
		if move_up {
			movement.y += 1.0;
		}
		if move_down {
			movement.y -= 1.0;
		}
		movement = movement.normalize_or_zero();

		// calculate movement
		let mut target_vel = velocity.x + movement.x * acceleration * dt;
		if target_vel.abs() > max_move_speed {
			target_vel = max_move_speed * movement.x.signum();
		}

		// apply movement
		velocity.x = target_vel;

		// jump
		if move_jump && grounded.on_ground {
			grounded.on_ground = false;
			velocity.y = 50.0;
		}

		// jump sustain
		else if move_up && velocity.y > 0.0 {
			let sustain_acc = gravity_component.gravity_scale * world_gravity.0 * -jump_sustain;
			velocity.0 += sustain_acc * time.delta_seconds();
		}

		// drop quickly
		else if move_down {
			let mut target_drop_vel = velocity.y + world_gravity.y * dt * 10.0;
			if target_drop_vel < max_drop_speed{
				target_drop_vel = max_drop_speed;
			}
			velocity.y = velocity.y.min(target_drop_vel);
		}

		let air_damp = 0.5;
		let ground_damp = 0.00000025;
		
		// adjust linear damping to account for ground friction
		if grounded.on_ground && !(move_left || move_right) {
			linear_damping.x = ground_damp;
		}
		else {
			linear_damping.x = air_damp;
		}

		linear_damping.update_data(&fixed_time);

		is_alive = true;
	}

	if is_alive {
		score.time += dt;
	}
}

fn handle_score_visualizer(
	score: Res<Score>, mut query: Query<&mut Transform, With<ScoreVisualizer>>
) {
	let mut transform = query.single_mut();
	transform.translation.y = score.height as f32 + 1.0;
}

fn handle_wrapping(mut query: Query<(&mut Transform, &WrapAroundX)>){
	for (mut transform, wrap) in &mut query {
		if transform.translation.x < -wrap.range {
			let left = transform.translation.x + wrap.range;
			transform.translation.x = wrap.range + left;
		}
		
		else if transform.translation.x > wrap.range {
			let right = transform.translation.x - wrap.range;
			transform.translation.x = -wrap.range + right;
		}
	}
}

fn handle_grounded_timer(fixed_time: Res<FixedTime>, mut query: Query<&mut GroundDetector>){
	for mut grounded in &mut query {
		if grounded.on_ground{
			grounded.ground_reset_timer -= fixed_time.period.as_secs_f32();
			if grounded.ground_reset_timer <= 0.0 {
				grounded.on_ground = false;
				grounded.ground_reset_timer = 0.0;
			}
		}
	}
}

fn handle_ground_detection(
	mut query: Query<(&mut GroundDetector, &collision::Collider, &collision::ColliderEvent)>
) {
	for (mut grounded, collider, col_evt) in &mut query {
		let collider_center = collider.center();
		for col in &col_evt.collisions {
			// if vertical collision
			if col.intersect.width() >= col.intersect.height() {
				// if ground collision
				if col.intersect.center().y < collider_center.y{
					grounded.on_ground = true;
					grounded.ground_reset_timer = 0.1;
					break;
				}
			}
		}
	}
}