use bevy::prelude::*;
use super::Velocity;

pub(super) struct LinearDampingPlugin;

impl Plugin for LinearDampingPlugin{
	fn build(&self, app: &mut App) {
		app.add_systems(Update, start_damping.before(handle_damping));
		app.add_systems(Update, handle_damping.after(super::gravity::handle_gravity));
	}
}

#[derive(Component, Deref, DerefMut)]
pub struct LinearDamping(pub Vec2);

impl LinearDamping{
	pub fn update_data(&mut self, fixed_time: &Res<FixedTime>) {
		self.0 = self.0.powf(fixed_time.period.as_secs_f32());
	}
}

impl Default for LinearDamping{
	fn default() -> Self {
		LinearDamping(Vec2::ONE)
	}
}

fn start_damping(time: Res<FixedTime>, mut query: Query<&mut LinearDamping, Added<LinearDamping>>){
	// apply fixed timestep time to damping factor
	for mut damping in &mut query {
		damping.update_data(&time);
	}
}

fn handle_damping(mut query: Query<(&LinearDamping, &mut Velocity)>){
	for (damping, mut velocity) in &mut query {
		velocity.0 *= damping.0;
	}
}