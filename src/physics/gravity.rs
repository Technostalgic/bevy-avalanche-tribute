use bevy::prelude::*;
use super::Velocity;

#[derive(Resource, Deref, DerefMut)]
pub struct GravityAcceleration(pub Vec2);

pub(super) struct GravityPlugin{
	pub gravity_acceleration: Vec2
}

impl Default for GravityPlugin{
	fn default() -> Self {
		GravityPlugin { gravity_acceleration: Vec2::new(0.0, -10.0) }
	}
}

impl Plugin for GravityPlugin{
	fn build(&self, app: &mut App) {
		app.insert_resource(GravityAcceleration(self.gravity_acceleration));
		app.add_systems(Update, handle_gravity);
	}
}

#[derive(Component)]
pub struct Gravity{
	pub gravity_scale: f32
}

impl Default for Gravity {
	fn default() -> Self {
		Gravity { gravity_scale: 1.0 }
	}
}

pub(super) fn handle_gravity(
	time: Res<FixedTime>,
	gravity_acceleration: Res<GravityAcceleration>,
	mut query: Query<(&Gravity, &mut Velocity)>
) {
	let dt = time.period.as_secs_f32();
	for (gravity, mut velocity) in &mut query {
		velocity.x += gravity_acceleration.x * gravity.gravity_scale * dt;
		velocity.y += gravity_acceleration.y * gravity.gravity_scale * dt;
	}
}