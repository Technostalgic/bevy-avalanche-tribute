use std::time::Duration;
use bevy::{prelude::*, window::WindowResolution};
use collision::Collider;
use player::{Controller, Score};

mod physics;
mod player;
mod collision;
mod falling_box;
mod lava;

fn main(){
	let mut app: App = App::new();
	app.add_plugins((
		DefaultPlugins,
		physics::PhysicsPlugin,
		collision::CollisionPlugin,
		player::PlayerPlugin,
		falling_box::FallingBoxPlugin,
		lava::LavaPlugin
	));
	app.add_systems(Startup, initialize);
	app.add_systems(Update, handle_camera);
	app.add_systems(Update, handle_score_text.after(handle_camera));
	app.run();
}

fn initialize(
	mut commands: Commands, mut fixed_time: ResMut<FixedTime>, mut windows: Query<&mut Window>
) {

	let mut window = windows.single_mut();
	window.resizable = false;
	window.resolution = WindowResolution::new(400.0, 720.0);

	// set fixed time interval
	fixed_time.period = Duration::from_secs_f32(1.0 / 60.0);

	// camera clear color
	let color_bg = Color::Rgba { 
		red: 0.1, green: 0.1, blue: 0.1, alpha: 1.0
	};

	// create the camera
	commands.spawn(Camera2dBundle{
		camera_2d: Camera2d{
			clear_color: bevy::core_pipeline::clear_color::ClearColorConfig::Custom(color_bg),
			..Default::default()
		},
		projection: OrthographicProjection{
			scale: 1.0 / 10.0,
			far: 1000.0,
			near: -1000.0,
			viewport_origin: Vec2::new(0.5, 0.5),
			..Default::default()
		},
		..Default::default()
	});

	// ground
	commands.spawn(collision::StaticColliderBundle{
		sprite_bundle: SpriteBundle{
			transform: Transform { 
				translation: Vec3::new(0.0, -0.0, 0.0),
				scale: Vec3::new(50.0, 2.0, 1.0), 
				..Default::default()
			},
			sprite: Sprite{
				color: Color::WHITE,
				..Default::default()
			},
			..Default::default()
		},
		collider: Collider(Rect::default()),
		..Default::default()
	});

	commands.spawn(
		Text2dBundle{
			transform: Transform {
				translation: Vec3::new(-10.0, 10.0, 2.0),
				scale: Vec3::new(0.1, 0.1, 1.0),
				..Default::default()
			},
			text: Text {
				sections: vec![TextSection::new(
					"Score: 0", 
					TextStyle { 
						font_size: 20.0, 
						color: Color::DARK_GREEN, 
						..Default::default() 
					})
				],
				..Default::default()
			},
			..Default::default()
	});
}

fn handle_camera(
	time: Res<Time>,
	mut cam_query: Query<&mut Transform, With<Camera>>, 
	target_query: Query<&Transform, (With<Controller>, Without<Camera>)>
) {
	let mut cam_transform = cam_query.single_mut();
	let targ = target_query.single();

	focus_camera(&mut cam_transform, targ.translation, time.delta_seconds());
}

fn focus_camera(cam_transform: &mut Transform, target: Vec3, dt: f32){

	let min_y: f32 = 15.0;
	let target_y = min_y.max(target.y);
	let factor: f32 = (0.1 as f32).powf(dt * 60.0);
	let dif = target_y - cam_transform.translation.y;
	
	let final_y = cam_transform.translation.y + dif * factor;
	cam_transform.translation.y = final_y;
}

fn handle_score_text(
	score: Res<Score>, mut text_query: Query<(&mut Text, &mut Transform)>, 
	cam_query: Query<(&Camera, &GlobalTransform), With<Camera>>
) {

	let mut text_tup = text_query.single_mut();
	text_tup.0.sections[0].value = format!("Score: {}", score.height);

	let cam_tup = cam_query.single();
	let top_left_ray = cam_tup.0.viewport_to_world(cam_tup.1, Vec2::ZERO);

	if let Some(top_left_ray) = top_left_ray {
		let mut top_left = top_left_ray.origin;
		top_left.x += 5.0;
		top_left.y -= 2.0;
		top_left.z = 2.0;
		text_tup.1.translation = top_left;
	}
}