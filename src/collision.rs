use bevy::prelude::*;
use crate::physics::Velocity;

pub struct CollisionPlugin;

impl Plugin for CollisionPlugin {
	fn build(&self, app: &mut App) {
		app.add_systems(FixedUpdate, start_collider.before(check_collisions));
		app.add_systems(FixedUpdate, reset_collider_events.before(check_collisions));
		app.add_systems(FixedUpdate, check_collisions);
		app.add_systems(FixedUpdate, check_collisions_dynamic.after(check_collisions));
		// app.add_systems(FixedUpdate, remove_unused_colliders.after(check_collisions_dynamic));
	}
}

#[derive(Component, Deref, DerefMut)]
pub struct Collider(pub Rect);

#[derive(Component)]
pub struct ColliderEvent{
	pub on_static_collide: Option<fn(
		commands: &mut Commands,
		col: &Collision, 
		self_entity: &Entity,
	)>,
	pub collisions: Vec<Collision>
}

#[derive(Component)]
pub struct DynamicColliderReaction;

#[derive(Bundle)]
pub struct StaticColliderBundle {
	pub sprite_bundle: SpriteBundle,
	pub collider: Collider
}

impl Default for StaticColliderBundle {
	fn default() -> Self {
		StaticColliderBundle { sprite_bundle: default(), collider: Collider(Rect::default()) }
	}
}

impl Collider {
	pub fn overlaps(&self, other: &Rect) -> Option<Collision> {
		return get_collision(self, &other);
	}
}

pub fn check_collisions(
	mut commands: Commands,
	mut dynamic_colliders: Query<(&mut Collider, &mut Velocity, &mut Transform, Entity)>,
	static_colliders: Query<&Collider, Without<Velocity>>,
	mut collider_events: Query<&mut ColliderEvent>
) {
	for (mut collider, mut velocity, mut transform, entity) in &mut dynamic_colliders {
		
		update_collider(&mut collider, &transform);
		for collider_static in &static_colliders {

			// get the collision between the static and the dynamic collider, if there is one
			let collision = get_collision(&collider, collider_static);
			if let Some(collision) = collision {

				resolve_static_collision(&collision, &mut transform, &mut velocity);
				update_collider(&mut collider, &transform);

				// get collider event if entity has one
				if let Ok(mut evt) = collider_events.get_mut(entity) {
					evt.collisions.push(collision);
					if let Some(col_listener) = evt.on_static_collide {
						col_listener(&mut commands, &collision, &entity);
					}
				}
			}
		}
	}
}

fn check_collisions_dynamic(
	mut commands: Commands,
	mut dynamic_reactors: Query<(&mut Collider, &mut Velocity, &mut Transform, Entity), With<DynamicColliderReaction>>,
	dynamic_colliders: Query<(&Collider, &Velocity), Without<DynamicColliderReaction>>,
	mut collider_events: Query<&mut ColliderEvent>
){
	for (mut collider, mut velocity, mut transform, entity) in &mut dynamic_reactors {
		for (dynamic_collider, dynamic_collider_vel) in &dynamic_colliders {

			let collision = get_collision(&collider, &dynamic_collider);
			if let Some(collision) = collision {

				resolve_dynamic_collision(&collision, &mut transform, &mut velocity, &dynamic_collider_vel);
				update_collider(&mut collider, &transform);
				if let Ok(mut evt) = collider_events.get_mut(entity) {
					evt.collisions.push(collision);
					if let Some(col_listener) = evt.on_static_collide {
						col_listener(&mut commands, &collision, &entity);
					}
				}
			}
		}
	}
}

// TODO - make this work properly. Currently it removes colliders that still can be used which
//		can cause falling blocks to fall forever if the collider that it would have collided 
//		with gets removed
// fn remove_unused_colliders(
// 	mut commands: Commands, lava: Res<lava::LavaLevel>, 
// 	query: Query<(Entity, &Collider), Without<Velocity>>
// ) {
// 
// 	for (entity, collider) in &query {
// 		if collider.max.y < lava.level {
// 			println!("Removed Collider");
// 			commands.entity(entity).remove::<Collider>();
// 		}
// 	}
// }

fn start_collider(mut query: Query<(&mut Collider, &Transform), Added<Collider>>){
	for (mut collider, transform) in &mut query {
		update_collider(&mut collider, transform);
	}
}

fn update_collider(collider: &mut Collider, transform: &Transform) {

	let mut min = Vec3::new(-0.5, -0.5, 0.0);
	let mut max = Vec3::new(0.5, 0.5, 0.0);

	min = transform.transform_point(min);
	max = transform.transform_point(max);

	collider.min.x = min.x;
	collider.min.y = min.y;
	collider.max.x = max.x;
	collider.max.y = max.y;
}

#[derive(Clone, Copy)]
pub struct Collision{
	pub intersect: Rect,
	pub normal: Vec2,
	pub depth: f32
}

fn get_collision(
	collider_a: &Collider, collider_b: &Rect
) -> Option<Collision> {
	
	if
		collider_a.min.x > collider_b.max.x ||
		collider_a.min.y > collider_b.max.y ||
		collider_a.max.x < collider_b.min.x ||
		collider_a.max.y < collider_b.min.y 
	{ 
		return None; 
	}

	// let vel_dif = *vel_b - *vel_a;
	let center_a = collider_a.center();

	let collider_intersect = Rect { 
		min: Vec2::new(
			collider_a.min.x.max(collider_b.min.x), 
			collider_a.min.y.max(collider_b.min.y)
		),
		max: Vec2::new(
			collider_a.max.x.min(collider_b.max.x), 
			collider_a.max.y.min(collider_b.max.y)
		) 
	};

	let mut center_dif: Vec2 = center_a - collider_intersect.center();
	center_dif = center_dif.normalize();

	let mut normal = Vec2::default();

	// horizontal collision
	if collider_intersect.width() < collider_intersect.height() {
		normal.x = center_dif.x.signum();
	}

	// vertical collision
	else {
		normal.y = center_dif.y.signum();
	}

	let depth = 
		collider_intersect.width() * normal.x.abs() + 
		collider_intersect.height() * normal.y.abs()
	;

	Some(Collision{
		intersect: collider_intersect,
		normal: normal,
		depth: depth
	})
}

fn resolve_static_collision(
	collision: &Collision, transform: &mut Transform, velocity: &mut Velocity
) {
	transform.translation += (collision.normal * collision.depth).extend(0.0);
	
	// horizontal
	if collision.normal.x.abs() > 0.0 {
		if velocity.x.signum() != collision.normal.x.signum(){
			velocity.x = 0.0;
		}
	}

	// vertical
	else {
		if velocity.y.signum() != collision.normal.y.signum(){
			velocity.y = 0.0;
		}
	}
}

fn resolve_dynamic_collision(
	collision: &Collision, transform: &mut Transform, velocity: &mut Velocity, other_vel: &Vec2
) {
	
	let collision_point = collision.intersect.center();

	// horizontal
	if collision.normal.x.abs() > 0.0 {
		if velocity.x.signum() != collision.normal.x.signum(){

			// left wall
			if collision_point.x > transform.translation.x { 
				velocity.x = velocity.x.min(other_vel.x);
			}

			// right wall
			else {
				velocity.x = velocity.x.max(other_vel.x);
			}
		}
	}

	// vertical
	else {

		// top wall
		if collision_point.y > transform.translation.y { 
			velocity.y = velocity.y.min(other_vel.y);
		}

		// ground
		else {
			velocity.y = velocity.y.max(other_vel.y);
		}
	}

	transform.translation += (collision.normal * collision.depth).extend(0.0);
}

fn reset_collider_events(
	mut query: Query<&mut ColliderEvent>
){
	for mut collider_evt in &mut query {
		collider_evt.collisions.clear();
	}
}